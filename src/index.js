import Gateway from "./gateway";

require('dotenv').config()

const microservices = Object.keys(process.env)
  .filter(key => key.match(/.+_API_URL$/))
  .map(urlKey => process.env[urlKey]); 

console.log(microservices);

const gateway = new Gateway({
  microservicesUrls: microservices,
  PORT: process.env.PORT,
  NODE_ENV: process.env.NODE_ENV
});

gateway.run();
