import {
	makeRemoteExecutableSchema,
	mergeSchemas,
	introspectSchema
} from "graphql-tools";
import {
	HttpLink
} from "apollo-link-http";
import fetch from "node-fetch";

// Koa libraries
import Koa from "koa";
import KoaRouter from "koa-router";
import KoaBodyParser from "koa-bodyparser";

// Apollo Server for Koa
import {
	graphqlKoa
} from "apollo-server-koa";


export default class Gateway {
	constructor({
		microservicesUrls,
		PORT,
		NODE_ENV
	}) {
		this.microservicesUrls = microservicesUrls;
		this.schemas = [];
		this.PORT = PORT;
		this.NODE_ENV = NODE_ENV;
		this.graphQLServer = new Koa();
		this.router = new KoaRouter();
		this.bodyParser = new KoaBodyParser();
	}

	async getSchema(uri) {
		let link = new HttpLink({
			uri,
			fetch
		});
		let schema = await introspectSchema(link);
		let executableSchema = makeRemoteExecutableSchema({
			schema,
			link
		});
		return executableSchema;
	}

	setupServer() {
		let schema = mergeSchemas({
			schemas: this.schemas
		});

		this.graphQLServer.use(this.bodyParser);
		this.graphQLServer.use(this.router.routes());
		this.graphQLServer.use(this.router.allowedMethods());


		const koaPlayground = require("graphql-playground-middleware-koa").default;
		this.router.all(
			"/playground",
			koaPlayground({
				endpoint: "/graphql"
			})
		);

		this.router.post(
			"/graphql",
			this.bodyParser,
			graphqlKoa({
				schema,
				tracing: true,
				cacheControl: true
			})
		);
		this.router.get(
			"/graphql",
			this.bodyParser,
			graphqlKoa({
				schema,
				tracing: true,
				cacheControl: true
			})
		);
	}

	createApp() {
		this.setupServer();
    this.graphQLServer.listen(this.PORT);

		console.log(
			`Running a GraphQL API Gateway at localhost:${this.PORT}/graphql`
		);
	}

	run() {
		this.schemas = this.microservicesUrls.map(async obj => {
			return await this.getSchema(obj);
		});

		Promise.all(this.schemas).then(results => {
			this.schemas = results;
			this.createApp();
		});
	}
}
