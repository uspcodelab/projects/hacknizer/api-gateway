REPO=jooaodanieel
PRFX=hacknizer-
TAG=latest
IMG=api-gateway


.PHONY: image push clean build

image:
	docker image build . -t ${REPO}/${PRFX}${IMG}:${TAG}

push:
	docker push ${REPO}/${PRFX}${IMG}:${TAG}

clean:
	docker image rm ${REPO}/${PRFX}${IMG}:${TAG}

build:
	make image
	make push
	make clean
